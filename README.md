# Play - Angular - Yeoman

## Goal
Our primary purpose is to create a stack that is based on a new generation of web technologies. The benifit has to include ease of development, dependency management (both backend and front end), auto pickup of source changes and produce a much leaner production application (both optimized in size and response).

Using play as the web framework if a matter of choice, mainly due to plays quick recompilation on source changes and the easy this brings to the development of backend processes. Play can easily be swapped for any app server (spring included) as our integration from back to front is over rest api`s into the angular stack.

Having clear seperation of layers is crutial in any web stack but having the integration of sbt with ability to call npm, bower and grunt commans from within the play console keeps everything nice and tidy. 

Based on work done by [Rohit Rai](https://github.com/milliondreams) - [Original Template](https://github.com/tuplejump/play-yeoman) with a couple of improvements.

* Changed from Scala to Java
* Upgrade play to 2.2.1 from 2.2.0
* Upgraded Angular dependencies from 1.0.8 to 1.2.0
* Removed [grunt-contrib-livereload](https://github.com/gruntjs/grunt-contrib-livereload) and [grunt-contrib-conenct](https://github.com/gruntjs/grunt-contrib-connect) in favour of [grunt-contrib-watch](https://github.com/gruntjs/grunt-contrib-watch)
* Swapped out [COMPASS](http://compass-style.org/) for [LESS](http://lesscss.org/)
* Wrote this awesome readme

## Stack

* [Play](http://www.playframework.com/)
* [Angular](angularjs.org)
* [Yeoman](http://yeoman.io/)


Yeoman is basically a templating framework that relies on grunt (like ant but for web apps) and bower (like apt-get but for web libs). It`s true power lies in creating a scaffold of this module....but time is not our friend my young padawan

### Indirectly needed

* [Nodejs](http://nodejs.org/)
* [Bower](http://bower.io/)
* [Grunt](http://gruntjs.com/)

![workflow](https://raw.github.com/yeoman/yeoman.io/gh-pages/media/workflow.jpg)

## Installation:
The installation process is a bit lenghty due to all the dependencies but definitly worth the end result

### [Play](http://www.playframework.com/)
[Download](http://www.playframework.com/download) a copy (anything in the 2.2.x range should be fine) and extract to a folder of choice. Remember to set your environmental variable PATH to include the path to the extracted play folder
example: PATH=$PATH;xxxxx;c:\dev\play2.2.1;

As i run multiple version of play, i normally rename the play.bat file to play22.bat for this perticular version.
It allows you to call play1, play21, play22 as different version of play within having to change paths the whole time.

### [Nodejs](http://nodejs.org/)
Download from above link and install, remember to click "set environment variable". This will register node and allow you to run from command line.

For those on Ubuntu, you need to add Chris Lea's PPA to get the latest version of node.js.
Add the the PPA and install like this:

```
sudo add-apt-repository ppa:chris-lea/node.js
sudo apt-get update
sudo apt-get install nodejs
```
Check that you have the latest version matching the download above by running:

```
node
\> console.log('Version: ' + process.version);
```
you should see the version installed:
```
Version: v0.10.22
```

open new terminal/console, type 
```
npm
```
check that it`s working (print out all npm details)

### [Grunt](http://gruntjs.com/)
```
npm install -g grunt-cli
```

### [Bower](http://bower.io/)
```
npm install -g bower
```

### [Yeoman](http://yeoman.io/)
```
npm install -g yo
```

### [Angular](angularjs.org)
Angular is installed from the play console by using bower to grab the needed dependencies. So no need to setup or download anything here

## Grab template & run
Now all the dependencies should be installed and ready to go, so grab a copy of this template.
```
git clone https://bitbucket.org/potgieterdl/pay sample
```

Once completed, change into the folder and run play

```
cd paytemplate
play22
```

This will load the sbt command line console, run the following
```
[pay] $ update
[pay] $ npm install
[pay] $ bower install
```

Generate project files for your ide (http://www.playframework.com/documentation/2.2.x/IDE)
for intellij
```
[pay] $ idea
```
for eclipse
```
[pay] $ eclipse
```

Use chrome browser and install [livereload](http://livereload.com/) plugin (this updates your browser when pages change)
[Chrome plugin](https://chrome.google.com/webstore/detail/livereload/jnihajbhpnppcggbcgedagnkighmdlei?hl=en-US)

Once completed you can run your application with
```
[pay] $ ~run
```

## How this hangs together
This whole setup allows for quick dev changes and fast reloads to mainly speend up your development. The learning section below breaks it down a bit with samples and video links to help you build the needed understanding.

## Learning and more info

### Play

* Short [video](http://vimeo.com/58969923)
* Long [video](http://www.youtube.com/watch?v=9_YYgl65FLs)
* [Twitter and play at scale](http://www.youtube.com/watch?v=8z3h4Uv9YbE)

### Yeoman

* Short [video](http://www.youtube.com/watch?v=zBt2g9ekiug&feature=c4-overview&list=UUfetJpmQH2XpFj8uFgWsezw)

### Grunt

* Short [video](http://www.youtube.com/watch?v=q3Sqljpr-Vc)
* Long [video](http://www.youtube.com/watch?v=bntNYzCrzvE)

### Bower

* Short [video](http://www.youtube.com/watch?v=HHNf512zM-M)
* Long [video](http://www.youtube.com/watch?v=o9Xo_WFAyqg)

### Angular

Start at lession 1....for [every video you will ever need](http://egghead.io/lessons)

### Angular and yeoman combined

* [Yeoman, Angular combined](http://www.youtube.com/watch?v=V_x14_62m3Q)

